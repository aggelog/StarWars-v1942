#ifndef __Player__
#define __Player__

#include "SDLGameObject.h"
#include "GameObjectFactory.h"

class Player : public SDLGameObject { // inherit from GameObject

public:

    Player() : SDLGameObject() { }

    virtual ~Player() { };

    virtual void draw();

    virtual void update();

    virtual void clean();

    virtual void collision();

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    int getRemainingLoops() const {
        return m_remainingLoops;
    }

    void setRemainingLoops(int remainingLoops) {
        m_remainingLoops = remainingLoops;
    }

    int getPlayerSpeed() const {
        return m_playerSpeed;
    }

    void setPlayerSpeed(int playerSpeed) {
        m_playerSpeed = playerSpeed;
    }

    int getBulletSpeed() const {
        return m_bulletSpeed;
    }

    void setBulletSpeed(int bulletSpeed) {
        m_bulletSpeed = bulletSpeed;
    }

    int getBulletFrequency() const {
        return m_bulletFrequency;
    }

    void setBulletFrequency(int bulletFrequency) {
        m_bulletFrequency = bulletFrequency;
    }

    virtual std::string type() { return "Player"; }

    bool getInvulnerable() {
        return m_invulnerable;
    }

    void setInvulnerable(bool invulnerable) {
        m_invulnerable = invulnerable;
    }

private:

    // handle any input from the keyboard, mouse, or joystick
    void handleInput();

    bool startLoop;
    bool loopAnimation;


    int m_startResurrection;
    int m_resurrectionPeriod;

    bool m_invulnerable;
    bool m_resurrecting;
    int m_bulletSpeed;
    int m_bulletFrequency;
    int m_playerSpeed;
    int m_remainingLoops;

    int bulletFiring;
    int lastFire;
};

class PlayerCreator : public BaseCreator {

    GameObject *createGameObject() const {
        return new Player();
    }
};

#endif //__Player__
