#ifndef __WinState__
#define __WinState__

#include <iostream>
#include <vector>
#include "MenuState.h"

class GameObject;

class WinState : public MenuState {
public:

    virtual ~WinState() { }

    virtual void update();

    virtual void render();

    virtual bool onEnter();

    virtual bool onExit();

    virtual std::string getStateID() const { return s_winID; }

    virtual void setCallbacks(const std::vector<Callback> &callbacks);

private:
    static void s_winToMain();

    static void s_restartPlay();

    static const std::string s_winID;
    std::vector<GameObject *> m_gameObjects;
};

#endif //__WinState__
