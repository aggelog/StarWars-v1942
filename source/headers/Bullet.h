#ifndef __Bullet__
#define __Bullet__

#include "SDLGameObject.h"

class PlayerBullet : public SDLGameObject {
public:
    PlayerBullet() : SDLGameObject(), m_heading(0, 0) {
        m_currentRow = 4;
        m_currentFrame = 5;
    }

    virtual  ~PlayerBullet() { }

    virtual std::string type() { return "PlayerBullet"; }

    virtual void load(std::unique_ptr<LoaderParams> pParams, Vector2D heading) {
        SDLGameObject::load(std::move(pParams));
        m_heading = heading;
    }

    virtual void draw() {
        SDLGameObject::draw();
    }

    virtual void collision() {
        m_bDead = true;
    }

    virtual void update() {
        m_velocity.setX(m_heading.getX());
        m_velocity.setY(m_heading.getY());

        SDLGameObject::update();
    }

    virtual void clean() {
        SDLGameObject::clean();
    }

private:
    Vector2D m_heading;
};


class EnemyBullet : public PlayerBullet {
public :
    EnemyBullet() : PlayerBullet() { }

    virtual ~EnemyBullet() { }

    virtual std::string type() { return "EnemyBullet"; }
};

#endif //__Bullet__