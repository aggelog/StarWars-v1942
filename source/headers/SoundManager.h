#ifndef __SoundManager__
#define __SoundManager__

#include <iostream>
#include <map>
#include <string>

#ifndef OSX
#include "SDL2/SDL_mixer.h"
#else

#include "SDL2_MIXER/SDL_mixer.h"

#endif

enum sound_type {
    SOUND_MUSIC = 0,
    SOUND_SFX = 1
};

class SoundManager {

public:

    static SoundManager *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new SoundManager();
            return s_pInstance;
        }
        return s_pInstance;
    }

    bool load(std::string fileName, std::string id, sound_type type);

    void playSound(std::string id, int loop);

    void playMusic(std::string id, int loop);

    int isPlayingMusic();

    int isMusicPaused();

    void pauseMusic();

    void resumeMusic();

    bool getRestart() { return m_restart; };

    void setRestart(bool val) { m_restart = val; };


private:

    static SoundManager *s_pInstance;

    std::map<std::string, Mix_Chunk *> m_sfxs;
    std::map<std::string, Mix_Music *> m_music;

    bool m_restart;

    SoundManager();

    ~SoundManager();
};

typedef SoundManager TheSoundManager;


#endif //__SoundManager__
