#ifndef __Level__
#define __Level__

#include <iostream>
#include <vector>
#include "Layer.h"
#include "LevelParser.h"
#include "Player.h"
#include "CollisionManager.h"

struct Tileset {
    int firstGridID;
    int tileWidth;
    int tileHeight;
    int spacing;
    int margin;
    int width;
    int height;
    int numColumns;
    int numRows;
    std::string name;
};

struct enemySettings {
    std::string type;
    int num;
    int hp;
    int movementSpeed;
    int frequencyBullet;
    int speedBullet;
    int spawnFrequency;
};

class Level {

public:

    ~Level();

    void update();

    void render();

    std::vector<Tileset> *getTilesets();

    std::vector<Layer *> *getLayers();

    std::vector<enemySettings> *getEnemySettings();

    struct enemySettings getEnemySettingsByType(std::string type);

    void decreaseEnemyNumberByType(int decreaseBy, std::string type);

    Player *getPlayer() { return m_pPlayer; }

    void setPlayer(Player *pPlayer) { m_pPlayer = pPlayer; }

    unsigned int getEndedAt() const { return m_end_at; }

    void setTotalRemainingEnemies();

private:

    friend class LevelParser;

    Level();

    Player *m_pPlayer;

    unsigned int m_end_at;

    std::vector<Layer *> m_layers;
    std::vector<Tileset> m_tilesets;
    std::vector<enemySettings> m_enemySettings;
};

#endif //__Level__
