#ifndef __MenuSelect__
#define __MenuSelect__

#include "SDLGameObject.h"
#include "GameObjectFactory.h"

class MenuSelect : public SDLGameObject {

public:
    MenuSelect() { };

    virtual ~MenuSelect() { };

    virtual void draw();

    virtual void update();

    virtual void clean();

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    void setCallback(void(*callback)()) { m_callback = callback; }

    int getCallbackID() { return m_callbackID; }

private:

    std::string m_spType;

    bool m_bReleased;
    int m_callbackID;

    void (*m_callback)();
};

class MenuSelectCreator : public BaseCreator {

    GameObject *createGameObject() const {
        return new MenuSelect();
    }
};

#endif //__MenuSelect__
