#ifndef __GameObjectFactory__
#define __GameObjectFactory__

#include <iostream>
#include <string>
#include <map>
#include "GameObject.h"

class BaseCreator {

public:

    virtual GameObject *createGameObject() const = 0;

    virtual ~BaseCreator() { }
};

class GameObjectFactory {

public:

    // create the public instance function
    static GameObjectFactory *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new GameObjectFactory();
            return s_pInstance;
        }
        return s_pInstance;
    }

    bool registerType(std::string typeID, BaseCreator *pCreator) {
        std::map<std::string, BaseCreator *>::iterator it = m_creators.find(typeID);
        // if the type is already registered, do nothing
        if (it != m_creators.end()) {
            delete pCreator;
            return false;
        }
        m_creators[typeID] = pCreator;
        return true;
    }

    GameObject *create(std::string typeID) {
        std::map<std::string, BaseCreator *>::iterator it =
                m_creators.find(typeID);
        if (it == m_creators.end()) {
            std::cout << "could not find type: " << typeID << "\n";
            return NULL;
        }
        BaseCreator *pCreator = (*it).second;
        return pCreator->createGameObject();
    }

private:

    GameObjectFactory() { }

    ~GameObjectFactory() { }

    // create the s_pInstance member variable
    static GameObjectFactory *s_pInstance;

    std::map<std::string, BaseCreator *> m_creators;
};


typedef GameObjectFactory TheGameObjectFactory;

#endif //__GameObjectFactory__
