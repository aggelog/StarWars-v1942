#ifndef __Game__
#define __Game__

#include <iostream>
#include <vector>
#include "TextureManager.h"
#include "GameObject.h"
#include "GameStateMachine.h"
#include "Level.h"

class Level;

class Game {
public:
    // create the public instance function
    static Game *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new Game();
            return s_pInstance;
        }
        return s_pInstance;
    }

    //Simply set the running variable to true
    bool init(const char *title, int xpos, int ypos, int width, int height, bool fullscreen, bool music, bool sfx);

    void render();

    void update();

    void handleEvents();

    void clean();

    SDL_Renderer *getRenderer() const { return m_pRenderer; }

    SDL_Window *getWindow() const { return m_pWindow; }

    GameStateMachine *getStateMachine() { return m_pGameStateMachine; }

    void registerAllGameTypes();

    void setPlayerLives(int lives) { m_playerLives = lives; }

    int getPlayerLives() { return m_playerLives; }

    void setCurrentLevel(int currentLevel);

    const int getCurrentLevel() { return m_currentLevel; }

    void setNextLevel(int nextLevel) { m_nextLevel = nextLevel; }

    const int getNextLevel() { return m_nextLevel; }

    void setLevelComplete(bool levelComplete) { m_bLevelComplete = levelComplete; }

    int getCurrentScore() { return m_currentScore; }

    void setCurrentScore(int score) { m_currentScore += score; }

    int getMaxScore() { return m_maxScore; }

    void setMaxScore(int score) { m_maxScore += score; }

    void setMusicMode(bool mode) { m_musicEnabled = mode; }

    void setSFXMode(bool mode) { m_sfxEnabled = mode; }

    bool isMusicEnabled() const { return m_musicEnabled; }

    bool isSFXEnabled() const { return m_sfxEnabled; }

    void setDifficulty(int difficulty) {
        m_difficulty = difficulty;
    }

    int getDifficulty() const { return m_difficulty; }

    const bool getLevelComplete() { return m_bLevelComplete; }

    bool running() { return m_bRunning; }

    void quit() { m_bRunning = false; }

    int getGameWidth() const { return m_gameWidth; }

    int getGameHeight() const { return m_gameHeight; }

    std::vector<std::string> getLevelFiles() { return m_levelFiles; }

    int getRemainingEnemies() { return m_remainingEnemies; }

    void decreaseRemainingEnemies() { --m_remainingEnemies; }

    int getTotalEnemies() { return m_totalEnemies; }

    void initTotalRemainingEnemies(int num) { m_totalEnemies = m_remainingEnemies = num; }

    int getTotalKilledEnemies() { return m_killedEnemies; }

    void setKilledEnemies() { ++m_killedEnemies; }

    void resetScoreKilled() { m_currentScore = m_maxScore = m_killedEnemies = 0; }

private:

    SDL_Window *m_pWindow;
    SDL_Renderer *m_pRenderer;

    GameStateMachine *m_pGameStateMachine;

    bool m_bRunning;

    bool m_musicEnabled;
    bool m_sfxEnabled;

    int m_difficulty;

    static Game *s_pInstance;

    int m_currentScore;
    int m_maxScore;

    int m_gameWidth;
    int m_gameHeight;
    int m_playerLives;

    int m_totalEnemies;
    int m_remainingEnemies;
    int m_killedEnemies;

    int m_currentLevel;
    int m_nextLevel;
    bool m_bLevelComplete;

    std::vector<std::string> m_levelFiles;

    Game();

    ~Game();
};


typedef Game TheGame;

#endif //__Game__
