#ifndef __OptionsMenuState__
#define __OptionsMenuState__

#include "MenuState.h"
#include "GameObject.h"

class OptionsMenuState : public MenuState {

public:

    virtual ~OptionsMenuState() { }

    virtual void update();

    virtual void render();

    virtual bool onEnter();

    virtual bool onExit();

    virtual std::string getStateID() const { return s_optionsMenuID; }

private:

    virtual void setCallbacks(const std::vector<Callback> &callbacks);

    // call back functions for menu items
    static void s_optionsToMainMenu();

    static void s_setDifficulty();

    static void s_setMusic();

    static void s_setSFX();

    static const std::string s_optionsMenuID;
    std::vector<GameObject *> m_gameObjects;
    std::string m_musicID;
};

#endif //__OptionsMenuState__
