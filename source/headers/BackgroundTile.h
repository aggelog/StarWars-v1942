#ifndef __BackgroundTile__
#define __BackgroundTile__

#include <iostream>
#include "GameObjectFactory.h"
#include "SDLGameObject.h"

class BackgroundTile : public SDLGameObject { // inherit from GameObject

public:

    BackgroundTile();

    virtual ~BackgroundTile() { };

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    virtual void draw();

    virtual void update();

    virtual void clean();

private:

};

class BackgroundTileCreator : public BaseCreator {

public:
    virtual GameObject *createGameObject() const {
        return new BackgroundTile();
    }
};

#endif //__BackgroundTile__
