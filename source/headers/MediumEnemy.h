#ifndef __MediumEnemy__
#define __MediumEnemy__

#include "Enemy.h"
#include "PlayState.h"

class MediumEnemy : public Enemy {
public:

    MediumEnemy(struct enemySettings settings);

    ~MediumEnemy() { };

    virtual void update();

    virtual void draw();

    virtual void collision();

private:
    int m_heading;
};

#endif //__MediumEnemy__
