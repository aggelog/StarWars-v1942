#ifndef __StateParser__
#define __StateParser__

#include <iostream>
#include <vector>
#include "../../lib/parsers/xml/tinyxml/tinyxml.h"
#include "GameObject.h"
#include "Level.h"

class StateParser {

public:

    StateParser() { }

    ~StateParser() { }

    bool parseState(const char *stateFile, std::string stateID, std::vector<GameObject *> *pObjects,
                    std::vector<std::string> *pTextureIDs, std::string &pMusicID,
                    std::vector<std::string> *pSfxIDs, std::vector<enemySettings> *pEnemySettings);

private:

    void parseObjects(TiXmlElement *pStateRoot, std::vector<GameObject *> *pObjects);

    void parseTextures(TiXmlElement *pStateRoot, std::vector<std::string> *pTextureIDs);

    void parseSounds(TiXmlElement *pStateRoot, std::string &pMusicID, std::vector<std::string> *pSfxIDs);

    void parseSettings(TiXmlElement *pStateRoot, std::vector<enemySettings> *pEnemySettings);
};

#endif //__StateParser__
