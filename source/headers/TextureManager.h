#ifndef __TextureManager__
#define __TextureManager__

#include <string>
#include <map>
#include "SDL2/SDL.h"
#ifndef OSX
    #include "SDL2/SDL_image.h"
#else
    #include "SDL2_IMAGE/SDL_image.h"
#endif

class TextureManager {

public:

    /*
     * A singleton is a class that can only have one instance.
     * This works for us, as we want to reuse the same TextureManager
     * throughout our game.
     */
    static TextureManager *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new TextureManager();
            return s_pInstance;
        }

        return s_pInstance;
    }

    bool load(std::string fileName, std::string id, SDL_Renderer *pRenderer);

    bool loadFromImage(std::string id, SDL_Texture * image, SDL_Renderer *pRenderer);

    void clearTextureMap();

    void clearFromTextureMap(std::string id);

    void draw(std::string id, int x, int y, int width, int height, SDL_Renderer *pRenderer,
              SDL_RendererFlip flip = SDL_FLIP_NONE, bool fullimage = false);

    void drawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame,
                   SDL_Renderer *pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);

    void drawTile(std::string id, int margin, int spacing, int x, int y, int width, int height, int currentRow,
                  int currentFrame, SDL_Renderer *pRenderer);

    std::map<std::string, SDL_Texture *> getTextureMap() { return m_textureMap; }

private:

    TextureManager() { }

    ~TextureManager() { }

    std::map<std::string, SDL_Texture *> m_textureMap;

    static TextureManager *s_pInstance;
};

typedef TextureManager TheTextureManager;


#endif //__TextureManager__
