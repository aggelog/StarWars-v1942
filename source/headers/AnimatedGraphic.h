#ifndef __AnimatedGraphic__
#define __AnimatedGraphic__

#include <iostream>
#include "GameObjectFactory.h"
#include "SDLGameObject.h"

class AnimatedGraphic : public SDLGameObject { // inherit from GameObject

public:

    AnimatedGraphic();

    virtual ~AnimatedGraphic() { };

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    virtual void draw();

    virtual void update();

    virtual void clean();

private:
    int m_animSpeed;
    int m_frameCount;
};

class AnimatedGraphicCreator : public BaseCreator {

public:
    virtual GameObject *createGameObject() const {
        return new AnimatedGraphic();
    }
};


#endif //__AnimatedGraphic__
