#ifndef __LargeEnemy__
#define __LargeEnemy__

#include "Enemy.h"
#include "PlayState.h"

class LargeEnemy : public Enemy {
public:

    LargeEnemy(struct enemySettings settings);

    ~LargeEnemy() { };

    virtual void update();

    virtual void draw();

    virtual void collision();

    Vector2D &getPositionLargeEnemy() {
        return m_position;
    }

    virtual void clean();

private:
    int m_heading;
    int bulletFiring;
    int lastFire;
};

#endif //__LargeEnemy__