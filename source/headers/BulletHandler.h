#ifndef __BulletHandler__
#define __BulletHandler__

#include "Bullet.h"

class BulletHandler {
public:
    static BulletHandler *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new BulletHandler();
            return s_pInstance;
        }

        return s_pInstance;
    }

    void addPlayerBullet(int x, int y, int width, int height, std::string textureID, int numFrames, Vector2D heading);

    void addEnemyBullet(int x, int y, int width, int height, std::string textureID, int numFrames, Vector2D heading);

    void update(std::string type);

    void draw(std::string type);

    const std::vector<PlayerBullet *> &getPlayerBullets() const {
        return m_playerBullets;
    }

    const std::vector<EnemyBullet *> &getEnemyBullets() const {
        return m_enemyBullets;
    }

    void cleanAllBullets() {
        for (int i = 0; i < m_playerBullets.size(); ++i) {
            m_playerBullets.erase(m_playerBullets.begin() + i);
        }
        m_playerBullets.clear();
        for (int i = 0; i < m_enemyBullets.size(); ++i) {
            m_enemyBullets.erase(m_enemyBullets.begin() + i);
        }
        m_enemyBullets.clear();
    }

private:
    BulletHandler() { }

    ~BulletHandler() { }

    static BulletHandler *s_pInstance;

    std::vector<PlayerBullet *> m_playerBullets;
    std::vector<EnemyBullet *> m_enemyBullets;

};

typedef BulletHandler TheBulletHandler;

#endif //__BulletHandler__