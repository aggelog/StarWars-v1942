#ifndef __Enemy__
#define __Enemy__

#include "SDLGameObject.h"
#include "GameObjectFactory.h"

class Enemy : public SDLGameObject {

public:

    Enemy() : SDLGameObject() { };

    virtual void draw();

    virtual void update();

    virtual void clean();

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);


public:
    int getHp() const {
        return m_hp;
    }

    void setHp(int hp) {
        m_hp = hp;
    }

    int getMovementSpeed() const {
        return m_movementSpeed;
    }

    void setMovementSpeed(int movementSpeed) {
        m_movementSpeed = movementSpeed;
    }

    int getFrequencyBullet() const {
        return m_frequencyBullet;
    }

    void setFrequencyBullet(int frequencyBullet) {
        m_frequencyBullet = frequencyBullet;
    }

    int getSpeedBullet() const {
        return m_speedBullet;
    }

    void setSpeedBullet(int speedBullet) {
        m_speedBullet = speedBullet;
    }

    virtual std::string type() { return "Enemy"; }

protected:

    virtual ~Enemy() { };

    int m_worthPoints;
    int m_hp;
    int m_movementSpeed;
    int m_frequencyBullet;
    int m_speedBullet;

};

class EnemyCreator : public BaseCreator {

    GameObject *createGameObject() const {
        return new Enemy();
    }
};

#endif //__Enemy__
