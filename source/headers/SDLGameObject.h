#ifndef __SDLGameObject__
#define __SDLGameObject__

#include <SDL2/SDL.h>
#include "GameObject.h"
#include "Vector2D.h"


class SDLGameObject : public GameObject {
public:

    virtual ~SDLGameObject() { }

    virtual void draw();

    virtual void update();

    virtual void clean();

    virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    Vector2D &getPosition() { return m_position; }

    int getWidth() { return m_width; }

    int getHeight() { return m_height; }

    virtual void collision() { }

    virtual std::string type() { return "SDLGameObject"; }

protected:

    SDLGameObject();

    Vector2D m_position;
    Vector2D m_velocity;
    Vector2D m_acceleration;

    int m_width;
    int m_height;

    int m_currentRow;
    int m_currentFrame;
    int m_prevFrame;

    int m_numFrames;
    bool m_bg;

    std::string m_textureID;
};

#endif //__SDLGameObject__
