#ifndef __SmallEnemy__
#define __SmallEnemy__

#include "Enemy.h"
#include "PlayState.h"

class SmallEnemy : public Enemy {
public:

    SmallEnemy(struct enemySettings settings);

    ~SmallEnemy() { };

    virtual void update();

    virtual void draw();

    virtual void collision();

};

#endif //__SmallEnemy__
