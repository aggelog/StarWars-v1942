#ifndef __GameStateMachine__
#define __GameStateMachine__

#include <vector>
#include "GameState.h"

class GameStateMachine {

public:

    GameStateMachine() { }

    ~GameStateMachine() { }

    void update();

    void render();

    void pushState(GameState *pState);

    void changeState(GameState *pState);

    void popState();

    void clean();

    std::vector<GameState *> &getGameStates() { return m_gameStates; }

    std::string getCurrentState() { return m_currentStateID; }

private:
    std::vector<GameState *> m_gameStates;
    std::string m_currentStateID;
};

#endif //__GameStateMachine__
