#ifndef __CollisionManager__
#define __CollisionManager__

#include <iostream>
#include <vector>
#include "Player.h"

class GameObject;

class CollisionManager {
public:

    void checkPlayerEnemyCollision(Player *pPlayer, const std::vector<GameObject *> &objects);

    void checkEnemyPlayerBulletCollision(Player *pPlayer, const std::vector<GameObject *> &objects);

    void checkPlayerEnemyBulletCollision(const std::vector<GameObject *> &objects);
};

#endif //__CollisionManager__
