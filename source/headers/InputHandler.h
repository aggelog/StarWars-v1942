#ifndef __InputHandler__
#define __InputHandler__

#include <iostream>
#include <vector>
#include "SDL2/SDL_events.h"
#include "Vector2D.h"

enum mouse_buttons {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2
};

class InputHandler {

public:

    static InputHandler *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new InputHandler();
        }
        return s_pInstance;
    }

    void update();

    void reset();

    void clean() { };

    // keyboard events
    bool isKeyDown(SDL_Scancode key) const;

    // mouse events
    bool getMouseButtonState(int buttonNumber) const;

    Vector2D *getMousePosition() const;

private:
    InputHandler();

    ~InputHandler();

    // handle keyboard events
    void onKeyDown() { };

    void onKeyUp() { };

    // handle mouse events
    void onMouseMove(SDL_Event &event);

    void onMouseButtonDown(SDL_Event &event);

    void onMouseButtonUp(SDL_Event &event);

    std::vector<bool> m_mouseButtonStates;
    Vector2D *m_mousePosition;
    const Uint8 *m_keystates;

    static InputHandler *s_pInstance;
};

typedef InputHandler TheInputHandler;

#endif //__InputHandler__
