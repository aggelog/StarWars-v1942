#ifndef __TextManager__
#define __TextManager__

#include <string>
#include "SDL2/SDL.h"

#ifndef OSX
#include "SDL2/SDL_image.h"
#include "SDL/SDL_ttf.h"
#else

#include "SDL2_IMAGE/SDL_image.h"
#include "SDL2_TTF/SDL_ttf.h"

#endif

class TextManager {

public:

    /*
     * A singleton is a class that can only have one instance.
     * This works for us, as we want to reuse the same TextureManager
     * throughout our game.
     */
    static TextManager *Instance() {
        if (s_pInstance == 0) {
            s_pInstance = new TextManager();
            return s_pInstance;
        }

        return s_pInstance;
    }

    int initTTF();

    SDL_Texture *renderText(const std::string &message, const std::string &fontFile,
                            SDL_Color color, int fontSize, SDL_Renderer *renderer);

private:

    TextManager() { }

    ~TextManager() { }

    static TextManager *s_pInstance;
};

typedef TextManager TheTextManager;

#endif //__TextManager__
