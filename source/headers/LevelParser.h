#ifndef __LevelParser__
#define __LevelParser__

#include "Level.h"
#include "../../lib/parsers/xml/tinyxml/tinyxml.h"

class Level;

struct Tileset;

class Layer;

class TileLayer;

class LevelParser {

public:

    LevelParser() { };

    ~LevelParser() { };

    Level *parseLevel(const char *levelFile);

private:

    void parseTextures(TiXmlElement *pTextureRoot);

    void parseTilesets(TiXmlElement *pTilesetRoot, std::vector<Tileset> *pTilesets);

    void parseObjectLayer(TiXmlElement *pObjectElement, std::vector<Layer *> *pLayers, Level *pLevel);

    void parseTileLayer(TiXmlElement *pTileElement, std::vector<Layer *> *pLayers,
                        const std::vector<Tileset> *pTilesets);

    int m_tileSize;
    int m_width;
    int m_height;

};

#endif //__LevelParser__
