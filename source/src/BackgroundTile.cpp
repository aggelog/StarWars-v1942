#include "BackgroundTile.h"

BackgroundTile::BackgroundTile() { }

void BackgroundTile::load(std::unique_ptr<LoaderParams> const &pParams) {
    SDLGameObject::load(pParams);
    m_bg = true;
}

void BackgroundTile::draw() {
    SDLGameObject::draw();
}

void BackgroundTile::update() { }

void BackgroundTile::clean() {
    SDLGameObject::clean();
}