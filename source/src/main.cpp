#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

// Initialization of easylogging service.
INITIALIZE_EASYLOGGINGPP

const int FPS = 60;
const int DELAY_TIME = (Uint32) (1000.0f / FPS);


int main(int argc, char *argv[]) {

    Uint32 frameStart, frameTime;

    LOG(INFO) << "[main.cpp] game init attempt...";

    // TODO: we can create a parser for Data-Driven initialization to avoid hardcoded values.ß
    if (TheGame::Instance()->init("StarWars-v1942", 100, 100, 1280, 720, false, true, false)) {
        LOG(INFO) << "[main.cpp] game init success!";

        // Game LOOP
        while (TheGame::Instance()->running()) {

            frameStart = SDL_GetTicks();

            TheGame::Instance()->handleEvents();
            TheGame::Instance()->update();
            TheGame::Instance()->render();

            frameTime = SDL_GetTicks() - frameStart;

            if (frameTime < DELAY_TIME) {
                SDL_Delay((Uint32) (DELAY_TIME - frameTime));
            }
        }
    }
    else {
        LOG(FATAL) << "[main.cpp] game init failure - " << SDL_GetError();
        return -1;
    }

    LOG(INFO) << "[main.cpp] game closing...";
    TheGame::Instance()->clean();
    return 0;
}
