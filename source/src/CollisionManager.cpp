#include "Enemy.h"
#include "BulletHandler.h"
#include "CollisionManager.h"
#include "Collision.h"
#include "TileLayer.h"
#include "../../lib/loggers/easylogging++.h"

void CollisionManager::checkPlayerEnemyCollision(Player *pPlayer, const std::vector<GameObject *> &objects) {

    SDL_Rect *pRect1 = new SDL_Rect();
    pRect1->x = (int) (pPlayer->getPosition().getX());
    pRect1->y = (int) (pPlayer->getPosition().getY());
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();

    for (int i = 0; i < objects.size(); i++) {
        //if ((objects[i])->type() != std::string("Enemy") || !objects[i]->updating())
        if ((objects[i])->type() != std::string("Enemy")) {
            continue;
        }

        SDL_Rect *pRect2 = new SDL_Rect();
        pRect2->x = (int) (dynamic_cast<Enemy *>(objects[i])->getPosition().getX());
        pRect2->y = (int) (dynamic_cast<Enemy *>(objects[i])->getPosition().getY());
        pRect2->w = dynamic_cast<Enemy *>(objects[i])->getWidth();
        pRect2->h = dynamic_cast<Enemy *>(objects[i])->getHeight();

        if (RectRect(pRect1, pRect2)) {
            if (!dynamic_cast<Enemy *>(objects[i])->dead() && !dynamic_cast<Enemy *>(objects[i])->dying()) {
                pPlayer->collision();
            }
        }

        delete pRect2;
    }

    delete pRect1;
}

void CollisionManager::checkEnemyPlayerBulletCollision(Player *pPlayer, const std::vector<GameObject *> &objects) {

    SDL_Rect *pRect1 = new SDL_Rect();
    pRect1->x = (int) pPlayer->getPosition().getX();
    pRect1->y = (int) pPlayer->getPosition().getY();
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();

    std::vector<EnemyBullet *> enemyBullets = TheBulletHandler::Instance()->getEnemyBullets();

    for (int i = 0; i < enemyBullets.size(); ++i) {

        SDL_Rect *pRect2 = new SDL_Rect();
        pRect2->x = (int) (dynamic_cast<EnemyBullet *>(enemyBullets[i])->getPosition().getX());
        pRect2->y = (int) (dynamic_cast<EnemyBullet *>(enemyBullets[i])->getPosition().getY());
        pRect2->w = dynamic_cast<EnemyBullet *>(enemyBullets[i])->getWidth();
        pRect2->h = dynamic_cast<EnemyBullet *>(enemyBullets[i])->getHeight();

        if (RectRect(pRect1, pRect2)) {
            if (!pPlayer->dead() && !pPlayer->dying() && !dynamic_cast<EnemyBullet *>(enemyBullets[i])->dead()) {
                enemyBullets[i]->collision();
                //delete enemyBullets[i];
                //enemyBullets.erase(enemyBullets.begin() + i);
                pPlayer->collision();
            }
        }
        delete pRect2;
    }
    delete pRect1;
}

void CollisionManager::checkPlayerEnemyBulletCollision(const std::vector<GameObject *> &objects) {

    std::vector<PlayerBullet *> playerBullets = TheBulletHandler::Instance()->getPlayerBullets();

    for (int i = 0; i < playerBullets.size(); i++) {
        SDL_Rect *pRect1 = new SDL_Rect();
        pRect1->x = (int) (dynamic_cast<PlayerBullet *>(playerBullets[i])->getPosition().getX());
        pRect1->y = (int) (dynamic_cast<PlayerBullet *>(playerBullets[i])->getPosition().getY());
        pRect1->w = dynamic_cast<PlayerBullet *>(playerBullets[i])->getWidth();
        pRect1->h = dynamic_cast<PlayerBullet *>(playerBullets[i])->getHeight();

        for (int j = 0; j < objects.size(); j++) {
            if (objects[j]->type() != std::string("Enemy")) {
                continue;
            }

            SDL_Rect *pRect2 = new SDL_Rect();
            pRect2->x = (int) (dynamic_cast<Enemy *>(objects[j])->getPosition().getX());
            pRect2->y = (int) (dynamic_cast<Enemy *>(objects[j])->getPosition().getY());
            pRect2->w = dynamic_cast<Enemy *>(objects[j])->getWidth();
            pRect2->h = dynamic_cast<Enemy *>(objects[j])->getHeight();


            if (RectRect(pRect1, pRect2)) {
                if (!dynamic_cast<Enemy *>(objects[j])->dead() &&
                    !dynamic_cast<Enemy *>(objects[j])->dying() &&
                    !dynamic_cast<PlayerBullet *>(playerBullets[i])->dead()) {

                    playerBullets[i]->collision();
                    //delete playerBullets[i];
                    //playerBullets.erase(playerBullets.begin() + i);
                    objects[j]->collision();
                }
            }

            delete pRect2;
        }

        delete pRect1;
    }
}