#include "SDLGameObject.h"
#include "TextureManager.h"
#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

SDLGameObject::SDLGameObject() : GameObject(), m_position(0, 0), m_velocity(0, 0), m_acceleration(0, 0) {
    m_bg = false;
}

void SDLGameObject::load(std::unique_ptr<LoaderParams> const &pParams) {
    m_position = Vector2D(pParams->getX(), pParams->getY());
    m_velocity = Vector2D(0, 0);
    m_acceleration = Vector2D(0, 0);
    m_width = pParams->getWidth();
    m_height = pParams->getHeight();
    m_textureID = pParams->getTextureID();
    m_currentRow = 1;
    m_currentFrame = 0;
    m_prevFrame = 0;
    m_numFrames = pParams->getNumFrames();
}

void SDLGameObject::draw() {

    if (m_bg) {
        TextureManager::Instance()->draw(m_textureID, (Uint32) m_position.getX(), (Uint32) m_position.getY(), m_width,
                                         m_height, TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, true);
        return;
    }


    TextureManager::Instance()->drawFrame(m_textureID,
                                          (Uint32) m_position.getX(), (Uint32) m_position.getY(),
                                          m_width, m_height, m_currentRow, m_currentFrame,
                                          TheGame::Instance()->getRenderer());
}

void SDLGameObject::update() {
    m_position += m_velocity;
    m_velocity += m_acceleration;
}

void SDLGameObject::clean() {
    delete this;
}