#include "GameOverState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

const std::string GameOverState::s_gameOverID = "GAMEOVER";

void GameOverState::update() {
    if (m_loadingComplete && !m_exiting && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->update();
        }
    }
}

void GameOverState::render() {
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->draw();
        }
    }
}

bool GameOverState::onEnter() {

    // parse the state
    StateParser stateParser;
    stateParser.parseState("../config/states.xml", s_gameOverID, &m_gameObjects, &m_textureIDList, m_musicID, nullptr,
                           nullptr);
    m_callbacks.push_back(0);
    m_callbacks.push_back(s_gameOverToMain);
    m_callbacks.push_back(s_restartPlay);
    // set the callbacks for menu items
    setCallbacks(m_callbacks);

    m_loadingComplete = true;
    if (!TheSoundManager::Instance()->isPlayingMusic()) {
        TheSoundManager::Instance()->playMusic(m_musicID, -1);
    }
    else {
        while (!Mix_FadeOutMusic(1500) && Mix_PlayingMusic()) {
            // wait for any fades to complete
            SDL_Delay(100);
        }
        TheSoundManager::Instance()->playMusic(m_musicID, -1);
    }

    return true;
}

bool GameOverState::onExit() {
    LOG(INFO) << "[GameOverState.cpp]-GameOverState::onExit()";
    m_exiting = true;
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            delete m_gameObjects[i];
            m_gameObjects.erase(m_gameObjects.begin() + i);
        }
    }
    m_gameObjects.clear();
    // clear the texture manager
    for (int i = 0; i < m_textureIDList.size(); i++) {
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }

    TheInputHandler::Instance()->reset();

    return true;
}

void GameOverState::setCallbacks(const std::vector<Callback> &callbacks) {
    // go through the game objects
    if (!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            // if they are of type MenuButton then assign a callback based on the id passed in from the file
            if (dynamic_cast<MenuButton *>(m_gameObjects[i])) {
                MenuButton *pButton = dynamic_cast<MenuButton *>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}

void GameOverState::s_gameOverToMain() {
    TheSoundManager::Instance()->setRestart(true);
    TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void GameOverState::s_restartPlay() {
    TheSoundManager::Instance()->setRestart(true);
    TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}