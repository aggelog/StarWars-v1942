#include "TextManager.h"
#include "../../lib/loggers/easylogging++.h"

TextManager *TextManager::s_pInstance = 0;

int TextManager::initTTF() {
    if (TTF_Init() != 0) {
        LOG(FATAL) << "[TextManager.cpp]-TextManager::TTF initialization failled";
        SDL_Quit();
        return 0;
    }
    return 1;
}

/**
* Render the message we want to display to a texture for drawing
* @param message The message we want to display
* @param fontFile The font we want to use to render the text
* @param color The color we want the text to be
* @param fontSize The size we want the font to be
* @param renderer The renderer to load the texture in
* @return An SDL_Texture containing the rendered message, or nullptr if something went wrong
*/
SDL_Texture *TextManager::renderText(const std::string &message, const std::string &fontFile,
                                     SDL_Color color, int fontSize, SDL_Renderer *renderer) {
    //Open the font
    TTF_Font *font = TTF_OpenFont(fontFile.c_str(), fontSize);
    if (font == nullptr) {
        LOG(ERROR) << "[TextManager.cpp]-TextManager::renderText TTF_OpenFont failled";
        return nullptr;
    }
    //We need to first render to a surface as that's what TTF_RenderText
    //returns, then load that surface into a texture
    SDL_Surface *surf = TTF_RenderText_Blended(font, message.c_str(), color);
    if (surf == nullptr) {
        TTF_CloseFont(font);
        LOG(ERROR) << "[TextManager.cpp]-TextManager::renderText TTF_RenderText failled";
        return nullptr;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surf);
    if (texture == nullptr) {
        LOG(ERROR) << "[TextManager.cpp]-TextManager::renderText CreateTexture failled";
    }
    //Clean up the surface and font
    SDL_FreeSurface(surf);
    TTF_CloseFont(font);
    return texture;
}