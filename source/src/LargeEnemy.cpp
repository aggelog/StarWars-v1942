#include "BulletHandler.h"
#include "LargeEnemy.h"
#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

LargeEnemy::LargeEnemy(struct enemySettings settings) {

    srand(time(NULL));

    int type = rand() % 10 + 1;
    if (type >= 8 && type <= 10) {
        m_velocity = Vector2D(0, -(settings.movementSpeed + 1));
        m_textureID = "largeEnemyBlue";
        m_width = 241;
        setHp(settings.hp + 1);
        setFrequencyBullet(settings.frequencyBullet - 500);
        setSpeedBullet(settings.speedBullet + 1);
        setMovementSpeed(settings.movementSpeed + 1);
    }
    else {
        m_velocity = Vector2D(0, -(settings.movementSpeed));
        m_textureID = "largeEnemyGreen";
        m_width = 266;

        setHp(settings.hp);
        setFrequencyBullet(settings.frequencyBullet);
        setSpeedBullet(settings.speedBullet);
        setMovementSpeed(settings.movementSpeed);
    }

    m_position = Vector2D(rand() % (TheGame::Instance()->getGameWidth() - 260), TheGame::Instance()->getGameHeight());
    m_currentRow = 1;
    m_currentFrame = 0;
    m_numFrames = 6;

    m_height = 200;
    m_worthPoints = 2000;
    TheGame::Instance()->setMaxScore(m_worthPoints);
    m_heading = 0;
    bulletFiring = lastFire = 0;
}

void LargeEnemy::update() {
    int gameWidth_tmp = TheGame::Instance()->getGameWidth();

    int movementSpeed_tmp = getMovementSpeed();

    int posY = (int) m_position.getY();
    int posX = (int) m_position.getX();


    if (posY == 0) {
        m_velocity.setY(0);
        if (m_heading == 0) {
            m_velocity.setX(movementSpeed_tmp);
            m_heading = 1;
        }

        if (posX >= gameWidth_tmp - 260) {
            m_heading = -1;
        }
        if (posX <= 0) {
            m_heading = 1;
        }
        m_velocity.setX(m_heading * movementSpeed_tmp);
    }


    if (!m_bDying) {
        bulletFiring = SDL_GetTicks();

        if (bulletFiring - lastFire >= getFrequencyBullet()) {
            TheBulletHandler::Instance()->addEnemyBullet(posX + 110, posY + 100, 35, 115, "largeEnemyBullet", 1,
                                                         Vector2D(-1, 1) * m_speedBullet);
            TheBulletHandler::Instance()->addEnemyBullet(posX + 110, posY + 100, 35, 115, "largeEnemyBullet", 1,
                                                         Vector2D(0, 1) * m_speedBullet);
            TheBulletHandler::Instance()->addEnemyBullet(posX + 110, posY + 100, 35, 115, "largeEnemyBullet", 1,
                                                         Vector2D(1, 1) * m_speedBullet);
            lastFire = bulletFiring;
        }
    }

    if (m_bDying && !m_bDead) {
        m_prevFrame = m_currentFrame;
        if (m_currentFrame == m_numFrames - 1) {
            m_bDead = true;
            m_bDying = false;
            return;
        }
        m_currentFrame = (SDL_GetTicks() / 200) % m_numFrames + 1;
        if ((m_currentFrame - m_prevFrame) > 1) {
            if(m_prevFrame == 0)
                ++m_prevFrame;
            m_currentFrame = m_prevFrame;
        }
    }

    Enemy::update();
}

void LargeEnemy::draw() {
    Enemy::draw();
}

void LargeEnemy::collision() {

    --m_hp;

    setHp(m_hp);

    if (m_hp == 0) {
        m_bDying = true;
        TheGame::Instance()->setCurrentScore(m_worthPoints);
        TheGame::Instance()->setKilledEnemies();
    }
}

void LargeEnemy::clean() {
    SDLGameObject::clean();
}
