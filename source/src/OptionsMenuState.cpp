#include "MenuSelect.h"
#include "MenuButton.h"
#include "Game.h"
#include "OptionsMenuState.h"
#include "MainMenuState.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

const std::string OptionsMenuState::s_optionsMenuID = "OPTIONS";

void OptionsMenuState::update() {
    if (m_loadingComplete && !m_exiting &&!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->update();
        }
    }
}

void OptionsMenuState::render() {
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->draw();
        }
    }
}

bool OptionsMenuState::onEnter() {
    // parse the state
    StateParser stateParser;
    stateParser.parseState("../config/states.xml", s_optionsMenuID, &m_gameObjects, &m_textureIDList, m_musicID,
                           nullptr, nullptr);
    m_callbacks.push_back(0);
    m_callbacks.push_back(s_setDifficulty);
    m_callbacks.push_back(s_setMusic);
    m_callbacks.push_back(s_setSFX);
    m_callbacks.push_back(s_optionsToMainMenu);

    setCallbacks(m_callbacks);

    m_loadingComplete = true;

    return true;
}

bool OptionsMenuState::onExit() {
    LOG(INFO) << "[OptionsMenuState.cpp]-OptionsMenuState::onExit()";
    m_exiting = true;
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->clean();
            m_gameObjects[i] = NULL;
        }
    }
    m_gameObjects.clear();
    // clear the texture manager
    for (int i = 0; i < m_textureIDList.size(); i++) {
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }

    TheInputHandler::Instance()->reset();

    return true;
}

void OptionsMenuState::setCallbacks(const std::vector<Callback> &callbacks) {
    // go through the game objects
    if (!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            // if they are of type MenuSelect then assign a callback based
            //on the id passed in from the file
            if (dynamic_cast<MenuSelect *>(m_gameObjects[i])) {
                MenuSelect *pSelect = dynamic_cast<MenuSelect *>(m_gameObjects[i]);
                pSelect->setCallback(callbacks[pSelect->getCallbackID()]);
            }
            else if (dynamic_cast<MenuButton *>(m_gameObjects[i])) {
                MenuButton *pButton = dynamic_cast<MenuButton *>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}

void OptionsMenuState::s_optionsToMainMenu() {
    TheSoundManager::Instance()->setRestart(false);
    TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void OptionsMenuState::s_setDifficulty() {

    int difficulty = TheGame::Instance()->getDifficulty();

    if (difficulty == 4) {
        TheGame::Instance()->setDifficulty(0);
    }
    else {
        TheGame::Instance()->setDifficulty(difficulty);
    }

}

void OptionsMenuState::s_setMusic() {

    bool mode = TheGame::Instance()->isMusicEnabled();

    if (mode && TheSoundManager::Instance()->isPlayingMusic()) {
        TheSoundManager::Instance()->pauseMusic();
    }

    if (!mode && TheSoundManager::Instance()->isMusicPaused()) {
        TheSoundManager::Instance()->resumeMusic();
    }

    if (!mode && !TheSoundManager::Instance()->isPlayingMusic()) {
        TheSoundManager::Instance()->playMusic(std::string("menuMusic"), -1);
    }

    TheGame::Instance()->setMusicMode(!mode);
}

void OptionsMenuState::s_setSFX() {

    bool mode = TheGame::Instance()->isSFXEnabled();
    TheGame::Instance()->setSFXMode(!mode);
}