#include "MediumEnemy.h"
#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

MediumEnemy::MediumEnemy(struct enemySettings settings) : Enemy() {


    srand(time(NULL));

    int typeA = rand() % 30 + 1;
    int typeB = rand() % 10 + 1;

    if (typeA <= 9) {                       // First Type
        m_position = Vector2D(rand() % (TheGame::Instance()->getGameWidth() - 135), -130);
        m_velocity = Vector2D(0, settings.movementSpeed);

        m_heading = 0;
        m_height = 130;
        m_numFrames = 22;
        m_currentRow = 1;
        if (typeB <= 5) {                   // Green
            m_textureID = "mediumEnemyGreenFirst";
            m_width = 136;
            setHp(settings.hp);
            setMovementSpeed(settings.movementSpeed);
        }
        else {                              // Blue
            m_textureID = "mediumEnemyBlueFirst";
            m_width = 128;
            setHp(settings.hp + 1);
            setMovementSpeed(settings.movementSpeed + 1);
        }
    }
    else if (typeA >= 10 && typeA <= 19) {  // Second Type
        m_position = Vector2D(rand() % (TheGame::Instance()->getGameWidth() - 120),
                              TheGame::Instance()->getGameHeight());
        m_velocity = Vector2D(0, -(settings.movementSpeed));
        m_width = 120;
        m_height = 99;
        m_numFrames = 7;
        m_currentRow = 1;
        if (typeB <= 5) {                   // Green
            m_textureID = "mediumEnemyGreenSecond";

            setHp(settings.hp);
            setMovementSpeed(settings.movementSpeed);
        }
        else {                              // Blue
            m_textureID = "mediumEnemyBlueSecond";
            setHp(settings.hp + 1);
            setMovementSpeed(settings.movementSpeed + 1);
        }
    }
    else {                                  // Third Type
        m_position = Vector2D(rand() % (TheGame::Instance()->getGameWidth() - 125), -100);
        m_velocity = Vector2D(0, settings.movementSpeed);
        m_width = 135;
        m_height = 99;
        m_numFrames = 7;
        m_currentRow = 1;

        if (typeB <= 5) {                   // Green
            m_textureID = std::string("mediumEnemyGreenThird");

            setHp(settings.hp);
            setMovementSpeed(settings.movementSpeed);
        }
        else {                              // Blue
            m_textureID = std::string("mediumEnemyBlueThird");

            setHp(settings.hp + 1);
            setMovementSpeed(settings.movementSpeed + 1);
        }
    }
    m_worthPoints = 1500;
    TheGame::Instance()->setMaxScore(m_worthPoints);
    m_currentFrame = 0;
}

void MediumEnemy::update() {

    int veloc = getMovementSpeed();
    int gameHeight = TheGame::Instance()->getGameHeight();
    int gameWidth = TheGame::Instance()->getGameWidth();



    //First Medium Enemy Animation
    if ((m_textureID == std::string("mediumEnemyGreenFirst")) ||
        (m_textureID == std::string("mediumEnemyBlueFirst"))) {
        if (m_position.getY() >= gameHeight / 2) {

            if (m_currentFrame < 4) {
                m_currentFrame = (SDL_GetTicks() / 100) % 5;
            }
            else if (m_currentFrame == 4) {
                m_currentFrame = 4;
            }
            m_velocity.setY(0);
        }
        int posY = (int) m_position.getY();
        int posX = (int) m_position.getX();

        if ((m_heading == 0) && (m_currentFrame == 4)) {
            m_velocity.setX(veloc);
            m_heading = 1;
        }

        if (posX >= gameWidth - m_width) {
            m_velocity.setX(0);
            m_heading = 0;
            if (m_currentFrame < 8) {
                m_currentFrame = ((SDL_GetTicks() / 100) % 4) + 5;
            }
            else if (m_currentFrame == 8) {
                m_currentFrame = 8;
                m_velocity.setY(-veloc);
                m_heading = 0;
            }
        }
        if (posY <= (gameHeight / 3) && (m_currentFrame >= 8)) {
            m_velocity.setY(0);
            if (m_currentFrame < 12) {
                m_currentFrame = ((SDL_GetTicks() / 100) % 4) + 9;
            }
            else if (m_currentFrame == 12) {
                m_currentFrame = 12;
                m_heading = -1;
            }
        }
        if ((posX <= 0) && (m_currentFrame >= 12)) {
            m_heading = 0;
            if (m_currentFrame < 16) {
                m_currentFrame = ((SDL_GetTicks() / 100) % 4) + 13;
            }
            else if (m_currentFrame == 16) {
                m_currentFrame = 16;
                m_velocity.setY(veloc);
                m_heading = 0;
            }
        }
        m_velocity.setX(m_heading * veloc);
    }

    if (m_bDying && !m_bDead) {
        m_prevFrame = m_currentFrame;
        if ((m_textureID != std::string("mediumEnemyGreenFirst")) &&
            (m_textureID != std::string("mediumEnemyBlueFirst"))) {

            if (m_currentFrame == m_numFrames - 1) {
                m_bDead = true;
                m_bDying = false;
                return;
            }
            m_currentFrame = (SDL_GetTicks() / 200) % m_numFrames + 1;
            if ((m_currentFrame - m_prevFrame) > 1) {
                if(m_prevFrame == 0) {
                    ++m_prevFrame;
                }
                m_currentFrame = m_prevFrame;
            }
        }
        else {
            m_textureID = "explosions";
            m_numFrames = 6;
            if (m_currentFrame == m_numFrames - 1) {
                m_bDead = true;
                m_bDying = false;
                return;
            }
            m_currentFrame = (SDL_GetTicks() / 200) % m_numFrames + 1;
            if ((m_currentFrame - m_prevFrame) > 1) {
                if(m_prevFrame == 0) {
                    ++m_prevFrame;
                }
                m_currentFrame = m_prevFrame;
            }
        }
    }

    Enemy::update();
}

void MediumEnemy::draw() {
    Enemy::draw();
}


void MediumEnemy::collision() {

    --m_hp;

    setHp(m_hp);

    if (m_hp == 0) {
        m_bDying = true;
        TheGame::Instance()->setCurrentScore(m_worthPoints);
        TheGame::Instance()->setKilledEnemies();
    }
}
