#include <iostream>
#include "BulletHandler.h"
#include "Player.h"
#include "InputHandler.h"
#include "../../lib/loggers/easylogging++.h"
#include "Game.h"

void Player::draw() {
    TheBulletHandler::Instance()->draw("player");
    SDLGameObject::draw();
}

void Player::load(std::unique_ptr<LoaderParams> const &pParams) {
    SDLGameObject::load(std::move(pParams));
    loopAnimation = false;
    startLoop = false;
    m_invulnerable = false;
    m_resurrecting = false;
    m_resurrectionPeriod = 2000;

    m_position.setX(TheGame::Instance()->getGameWidth() / 2 - (m_width / 2));
    m_position.setY(TheGame::Instance()->getGameHeight() - 145);
}

void Player::handleInput() {
    int tmpWidth = TheGame::Instance()->getGameWidth() - 128;
    int tmpHeight = TheGame::Instance()->getGameHeight() - 145;

    int tmpX = (int) m_position.getX();
    int tmpY = (int) m_position.getY();

    //Movement
    if (!TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT)) {
        m_velocity.setX(0);
    }
    if (!TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT)) {
        m_velocity.setX(0);
    }
    if (!TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP)) {
        m_velocity.setY(0);
    }
    if (!TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN)) {
        m_velocity.setY(0);
    }

    if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT)) {
        m_velocity.setX(m_playerSpeed);
    }
    if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT)) {
        m_velocity.setX(-m_playerSpeed);
    }
    if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP)) {
        m_velocity.setY(-m_playerSpeed);
    }
    if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN)) {
        m_velocity.setY(m_playerSpeed);
    }


    if (tmpX < 0) {
        m_position.setX(0);
    }
    else if (tmpX > tmpWidth) {
        m_position.setX(tmpWidth);
    }
    if (tmpY < 0) {
        m_position.setY(0);
    }
    else if (tmpY > tmpHeight) {
        m_position.setY(tmpHeight);
    }

    //Bullet Firing
    if (TheInputHandler::Instance()->getMouseButtonState(0)) {

        bulletFiring = SDL_GetTicks();

        if (((bulletFiring - lastFire) > m_bulletFrequency) && !loopAnimation && !m_resurrecting) {
            TheBulletHandler::Instance()->addPlayerBullet((int) m_position.getX() + 10, (int) (m_position.getY() - 15),
                                                          35, 115, "bullet", 1,
                                                          Vector2D(0, -m_bulletSpeed));
            TheBulletHandler::Instance()->addPlayerBullet((int) (m_position.getX() + 90),
                                                          (int) (m_position.getY() - 15), 35, 115, "bullet",
                                                          1,
                                                          Vector2D(0, -m_bulletSpeed));
        }
        lastFire = bulletFiring;
    }

    //Loop Animation
    if (TheInputHandler::Instance()->getMouseButtonState(RIGHT) && !m_resurrecting) {
        if ((m_remainingLoops > 0) && (!loopAnimation)) {
            startLoop = true;
            setInvulnerable(true);
        }
    }

    if (!TheInputHandler::Instance()->getMouseButtonState(RIGHT) && startLoop) {
        loopAnimation = true;
        --m_remainingLoops;
        startLoop = false;
    }


}

void Player::update() {

    handleInput();

    int prevFrame = m_currentFrame;
    m_currentFrame = 0;
    if (loopAnimation) {
        m_currentFrame = int(((SDL_GetTicks() / 600)) % (m_numFrames + 1));
        if ((m_currentFrame - prevFrame) > 1) {
            if(prevFrame == 0)
                ++prevFrame;
            m_currentFrame = prevFrame;
        }
        if (m_currentFrame == m_numFrames) {
            loopAnimation = false;
            m_currentFrame = 0;
            setInvulnerable(false);
        }
    }

    //Resurrect Animation
    int currTime = SDL_GetTicks();
    if ((currTime - m_startResurrection >= m_resurrectionPeriod) && m_resurrecting) {
        setInvulnerable(false);
        m_resurrecting = false;
        m_textureID = "loopAnimation";
        m_width = 130;
        m_height = 66;
        m_currentFrame = 0;
        m_numFrames = 5;
    }
    if (m_resurrecting) {
        m_textureID = "explosions";
        m_width = 136;
        m_height = 130;
        m_numFrames = 6;
        m_currentFrame = (SDL_GetTicks() / 200) % m_numFrames;
    }


    TheBulletHandler::Instance()->update("player");
    SDLGameObject::update();
}

void Player::clean() {
    SDLGameObject::clean();
}

void Player::collision() {
    int playerLives = TheGame::Instance()->getPlayerLives();

    if (!m_invulnerable) {
        --playerLives;
        setInvulnerable(true);
        m_resurrecting = true;
        m_startResurrection = SDL_GetTicks();
    }

    if (playerLives >= 0) {
        TheGame::Instance()->setPlayerLives(playerLives);

    }
}