#include "StateParser.h"
#include "Game.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

bool StateParser::parseState(const char *stateFile, std::string stateID, std::vector<GameObject *> *pObjects,
                             std::vector<std::string> *pTextureIDs, std::string &pMusicID,
                             std::vector<std::string> *pSfxIDs, std::vector<enemySettings> *pEnemySettings) {

    // create the XML document
    TiXmlDocument xmlDoc;

    // load the state file
    if (!xmlDoc.LoadFile(stateFile)) {
        LOG(ERROR) << "[StateParser.cpp]-StateParser::parseState Coudn't load the state file with error: " <<
        xmlDoc.ErrorDesc();
        return false;
    }

    // get the root element
    TiXmlElement *pRoot = xmlDoc.RootElement();

    // pre declare the states root node
    TiXmlElement *pStateRoot = 0;

    // get this states root node and assign it to pStateRoot
    for (TiXmlElement *e = pRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == stateID) {
            pStateRoot = e;
            break;
        }
    }

    // pre declare the texture root
    TiXmlElement *pTextureRoot = 0;

    // get the root of the texture element
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("TEXTURES")) {
            pTextureRoot = e;
            break;
        }
    }

    // now parse the textures
    parseTextures(pTextureRoot, pTextureIDs);

    // pre declare the object root node
    TiXmlElement *pObjectRoot = 0;

    // get the root node and assign it to pObjectRoot
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("OBJECTS")) {
            pObjectRoot = e;
            break;
        }
    }

    // now parse the objects
    if (pObjects != nullptr)
        parseObjects(pObjectRoot, pObjects);

    // pre declare the sounds root
    TiXmlElement *pSoundsRoot = 0;

    // get the root of the sounds elemenet
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("SOUNDS")) {
            pSoundsRoot = e;
            break;
        }
    }

    // now parse the sounds
    if (pObjects != nullptr)
        parseSounds(pSoundsRoot, pMusicID, pSfxIDs);


    // pre declare the sounds root
    TiXmlElement *pSettingsRoot = 0;

    // get the root of the sounds elemenet
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("SETTINGS")) {
            pSettingsRoot = e;
            break;
        }
    }

    // now parse the sounds
    if (pEnemySettings != nullptr)
        parseSettings(pSettingsRoot, pEnemySettings);

    return true;
}


void StateParser::parseTextures(TiXmlElement *pStateRoot, std::vector<std::string> *pTextureIDs) {
/*
 * TODO: handle missing attributes assigned to strings.
 * TODO: If 0 is returned for e->Attribute(attrname) and assigned to std::string will cause Segmentation.
 */
    // for each texture we get the filename and the desired ID and add them to the texture manager
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {
        if (e->Value() == std::string("texture")) {
            std::string filenameAttribute = e->Attribute("filename");
            std::string idAttribute = e->Attribute("ID");

            pTextureIDs->push_back(idAttribute); // push into list

            TheTextureManager::Instance()->load(filenameAttribute, idAttribute, TheGame::Instance()->getRenderer());
        }
        else {
            LOG(WARNING) << "[StateParser.cpp]-StateParser::parseTextures unknown XML element: " << e->Value();
        }
    }
}


void StateParser::parseObjects(TiXmlElement *pStateRoot, std::vector<GameObject *> *pObjects) {
/*
 * TODO: handle missing attributes assigned to strings.
 * TODO: If 0 is returned for e->Attribute(attrname) and assigned to std::string will cause Segmentation.
 */
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {

        if (e->Value() == std::string("object")) {
            int x, y, width, height, numFrames, callbackID, animSpeed;
            std::string textureID, spType;

            e->Attribute("x", &x);
            e->Attribute("y", &y);
            e->Attribute("width", &width);
            e->Attribute("height", &height);
            e->Attribute("numFrames", &numFrames);
            e->Attribute("callbackID", &callbackID);
            e->Attribute("animSpeed", &animSpeed);

            textureID = e->Attribute("textureID");

            if (e->Attribute("type") == std::string("BackgroundTile")) {
                width = TheGame::Instance()->getGameWidth();
                height = TheGame::Instance()->getGameHeight();

                GameObject *pGameObject = TheGameObjectFactory::Instance()->create(e->Attribute("type"));

                pGameObject->load(
                        std::unique_ptr<LoaderParams>(new LoaderParams(x, y, width, height, textureID, numFrames)));
                pObjects->push_back(pGameObject);
                continue;
            }

            if (e->Attribute("type") == std::string("LabelTile")) {

                GameObject *pGameObject = TheGameObjectFactory::Instance()->create(std::string("BackgroundTile"));

                pGameObject->load(
                        std::unique_ptr<LoaderParams>(new LoaderParams(x, y, width, height, textureID, numFrames)));
                pObjects->push_back(pGameObject);
                continue;
            }

            if (e->Attribute("type") == std::string("MenuSelect")) {
                spType = e->Attribute("spType");
                GameObject *pGameObject = TheGameObjectFactory::Instance()->create(e->Attribute("type"));

                pGameObject->load(std::unique_ptr<LoaderParams>(
                        new LoaderParams(x, y, width, height, textureID, numFrames, callbackID, 0, spType)));
                pObjects->push_back(pGameObject);
                continue;
            }

            GameObject *pGameObject = TheGameObjectFactory::Instance()->create(e->Attribute("type"));

            pGameObject->load(std::unique_ptr<LoaderParams>(
                    new LoaderParams(x, y, width, height, textureID, numFrames, callbackID, animSpeed)));
            pObjects->push_back(pGameObject);
        }
        else {
            LOG(WARNING) << "[StateParser.cpp]-StateParser::parseObjects unknown XML element: " << e->Value();
        }
    }
}


void StateParser::parseSounds(TiXmlElement *pStateRoot, std::string &pMusicID,
                              std::vector<std::string> *pSfxIDs) {
/*
 * TODO: uncomment push_back attribute IDs when not empty pointers are given to parseState.
 */
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {

        std::string filenameAttribute;
        std::string idAttribute;

        if (e->Value() == std::string("music")) {
            filenameAttribute = e->Attribute("filename") == 0 ? "" : e->Attribute("filename");
            idAttribute = e->Attribute("ID") == 0 ? "" : e->Attribute("ID");

            if (filenameAttribute != "" || idAttribute != "") {

                pMusicID = idAttribute;

                TheSoundManager::Instance()->load(filenameAttribute, idAttribute, SOUND_MUSIC);
            }
            else {
                LOG(WARNING) << "[StateParser.cpp]-StateParser::parseSounds missing XML attribute";
            }

        }
        else if (e->Value() == std::string("sfx")) {
            filenameAttribute = e->Attribute("filename") == 0 ? "" : e->Attribute("filename");
            idAttribute = e->Attribute("ID") == 0 ? "" : e->Attribute("ID");

            if (filenameAttribute != "" || idAttribute != "") {
                //pSfxIDs->push_back(idAttribute); // push into list
                TheSoundManager::Instance()->load(filenameAttribute, idAttribute, SOUND_SFX);
            }
            else {
                LOG(WARNING) << "[StateParser.cpp]-StateParser::parseSounds missing XML attribute";
            }

        }
        else {
            LOG(WARNING) << "[StateParser.cpp]-StateParser::parseSounds unknown XML element: " << e->Value();
        }
    }
}

void StateParser::parseSettings(TiXmlElement *pStateRoot, std::vector<enemySettings> *pEnemySettings) {
    for (TiXmlElement *e = pStateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()) {

        // create a enemySettings object
        enemySettings enemySetting;
        int diffFactor = 1 * TheGame::Instance()->getDifficulty();

        enemySetting.type = e->Value();
        e->Attribute("movementSpeed", &enemySetting.movementSpeed);
        e->Attribute("frequencyBullet", &enemySetting.frequencyBullet);
        e->Attribute("speedBullet", &enemySetting.speedBullet);
        e->Attribute("num", &enemySetting.num);
        e->Attribute("hp", &enemySetting.hp);
        e->Attribute("spawnFrequency", &enemySetting.spawnFrequency);

        enemySetting.movementSpeed += diffFactor;
        enemySetting.frequencyBullet -= (diffFactor * 100);
        enemySetting.speedBullet += diffFactor;
        enemySetting.hp += diffFactor;

        pEnemySettings->push_back(enemySetting);
    }
}