#include "Level.h"
#include "Game.h"

Level::Level() {
    unsigned int min = 1;
    m_end_at = min * 60 * 1000 + SDL_GetTicks();
}

Level::~Level() {
    if (!m_layers.empty()) {
        for (int i = 0; i < m_layers.size(); i++) {
            delete m_layers[i];
        }
    }

    m_layers.clear();
}

void Level::render() {
    if (!m_layers.empty()) {
        for (int i = 0; i < m_layers.size(); i++) {
            m_layers[i]->render();
        }
    }
}

void Level::update() {
    if (!m_layers.empty()) {
        for (int i = 0; i < m_layers.size(); i++) {
            m_layers[i]->update(this);
        }
    }
}

std::vector<Tileset> *Level::getTilesets() {
    return &m_tilesets;
}

std::vector<Layer *> *Level::getLayers() {
    return &m_layers;
}

std::vector<enemySettings> *Level::getEnemySettings() {
    return &m_enemySettings;
}

struct enemySettings Level::getEnemySettingsByType(std::string type) {
    struct enemySettings p;
    for (int i = 0; i < m_enemySettings.size(); i++) {
        if (m_enemySettings[i].type == std::string(type)) {
            return m_enemySettings[i];
        }
    }
    return p;
}

void Level::decreaseEnemyNumberByType(int decreaseBy, std::string type) {
    for (int i = 0; i < m_enemySettings.size(); i++) {
        if (m_enemySettings[i].type == std::string(type)) {
            m_enemySettings[i].num -= decreaseBy;
            return;
        }
    }
}

void Level::setTotalRemainingEnemies() {
    int total = 0;
    for (int i = 0; i < m_enemySettings.size(); i++) {
        total += m_enemySettings[i].num;
    }
    TheGame::Instance()->initTotalRemainingEnemies(total);
}