#include "MenuSelect.h"
#include "InputHandler.h"
#include "SoundManager.h"
#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

void MenuSelect::load(std::unique_ptr<LoaderParams> const &pParams) {
    SDLGameObject::load(std::move(pParams));
    m_callbackID = pParams->getCallbackID();
    m_spType = pParams->getSpecialType();

    if (m_spType == std::string("difficulty")) {
        m_currentFrame = TheGame::Instance()->getDifficulty();
    }
    else if (m_spType == std::string("music")) {
        m_currentFrame = !TheGame::Instance()->isMusicEnabled();
    }
    else if (m_spType == std::string("sfx")) {
        m_currentFrame = !TheGame::Instance()->isSFXEnabled();
    }

    m_bReleased = true;
    m_bg = false;
}

void MenuSelect::draw() {
    SDLGameObject::draw(); // use the base class drawing
}

void MenuSelect::update() {
    Vector2D *pMousePos = TheInputHandler::Instance()->getMousePosition();

    if (pMousePos->getX() < (m_position.getX() + m_width) && pMousePos->getX() > m_position.getX() &&
        pMousePos->getY() < (m_position.getY() + m_height) && pMousePos->getY() > m_position.getY()) {

        if (TheInputHandler::Instance()->getMouseButtonState(LEFT) && m_bReleased) {
            m_bReleased = false;
        }
        else if (!TheInputHandler::Instance()->getMouseButtonState(LEFT) && !m_bReleased) {
            if (m_spType == std::string("difficulty")) {
                ++m_currentFrame;
                TheGame::Instance()->setDifficulty(m_currentFrame);
                if (m_currentFrame == 4) {
                    m_currentFrame = 0;
                    TheGame::Instance()->setDifficulty(m_currentFrame);
                }
            }
            else if (m_spType == std::string("music")) {
                if (m_currentFrame) {
                    m_currentFrame = 0;
                }
                else {
                    m_currentFrame = 1;
                }
            }
            else if (m_spType == std::string("sfx")) {
                if (m_currentFrame) {
                    m_currentFrame = 0;
                }
                else {
                    m_currentFrame = 1;
                }
            }
            m_callback(); // call our callback function
            m_bReleased = true;
        }
    }
    else {
        if (TheInputHandler::Instance()->getMouseButtonState(LEFT) && m_bReleased) {
            m_bReleased = false;
        }
        if (!TheInputHandler::Instance()->getMouseButtonState(LEFT) && !m_bReleased) {
            m_bReleased = true;
        }
    }
}

void MenuSelect::clean() {
    SDLGameObject::clean();
}