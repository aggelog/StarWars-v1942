#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

TextureManager *TextureManager::s_pInstance = 0;

bool TextureManager::load(std::string fileName, std::string id, SDL_Renderer *pRenderer) {

    SDL_Surface *pTempSurface = IMG_Load(fileName.c_str());

    if (pTempSurface == 0) {
        return false;
    }

    SDL_Texture *pTexture = SDL_CreateTextureFromSurface(pRenderer, pTempSurface);

    SDL_FreeSurface(pTempSurface);

    // everything went ok, add the texture to our list
    if (pTexture != 0) {
        m_textureMap[id] = pTexture;
        return true;
    }

    // reaching here means something went wrong
    return false;
}

bool TextureManager::loadFromImage(std::string id, SDL_Texture *image, SDL_Renderer *pRenderer) {
    if (image != 0) {
        m_textureMap[id] = image;
        return true;
    }
    return false;
}

void TextureManager::draw(std::string id, int x, int y, int width, int height, SDL_Renderer *pRenderer,
                          SDL_RendererFlip flip, bool fullimage) {
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = destRect.w = width;
    srcRect.h = destRect.h = height;
    destRect.x = x;
    destRect.y = y;

    if (fullimage) {
        SDL_RenderCopyEx(pRenderer, m_textureMap[id], nullptr, &destRect, 0, 0, flip);
    }
    else {
        SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
    }
}

void TextureManager::drawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame,
                               SDL_Renderer *pRenderer, SDL_RendererFlip flip) {

    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = width * currentFrame;
    srcRect.y = height * (currentRow - 1);
    srcRect.w = destRect.w = width;
    srcRect.h = destRect.h = height;
    destRect.x = x;
    destRect.y = y;


    SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
}

void TextureManager::clearTextureMap() {
    LOG(INFO) << "[TextureManager.cpp]-TextureManager::clearTextureMap()";
    m_textureMap.clear();
}

void TextureManager::clearFromTextureMap(std::string id) {
    LOG(INFO) << "[TextureManager.cpp]-TextureManager::clearFromTextureMap(std::string id)\tid = " << id;
    m_textureMap.erase(id);
}

void TextureManager::drawTile(std::string id, int margin, int spacing, int x, int y, int width, int height,
                              int currentRow, int currentFrame, SDL_Renderer *pRenderer) {
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = margin + (spacing + width) * currentFrame;
    srcRect.y = margin + (spacing + height) * currentRow;

    srcRect.w = destRect.w = width;
    srcRect.h = destRect.h = height;

    destRect.x = x;
    destRect.y = y;

    if (id == std::string("Player")) {
        LOG(INFO) << "Changing player texture alpha";
        SDL_SetTextureAlphaMod(m_textureMap[id], 200);
    }

    SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, SDL_FLIP_NONE);
}