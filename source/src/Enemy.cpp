#include <iostream>
#include "Enemy.h"
#include "BulletHandler.h"

void Enemy::draw() {
    TheBulletHandler::Instance()->draw("enemy");
    SDLGameObject::draw();
}

void Enemy::load(std::unique_ptr<LoaderParams> const &pParams) {
    SDLGameObject::load(std::move(pParams));

}

void Enemy::update() {
    TheBulletHandler::Instance()->update("enemy");
    SDLGameObject::update();
}

void Enemy::clean() {
    SDLGameObject::clean();
}