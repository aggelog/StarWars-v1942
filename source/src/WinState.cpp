#include "WinState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "TextManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

const std::string WinState::s_winID = "WIN";

void WinState::update() {
    if (m_loadingComplete && !m_exiting && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->update();
        }
    }
}

void WinState::render() {

    if (m_loadingComplete && !m_gameObjects.empty()) {
        TextureManager::Instance()->draw(std::string("WinTitle"), (Uint32) 250, (Uint32) 10, 600, 130,
                                         TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, false);
        TextureManager::Instance()->draw(std::string("DeadClones"), (Uint32) 200, (Uint32) 170, 325, 90,
                                         TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, false);
        TextureManager::Instance()->draw(std::string("KilledText"), (Uint32) 650, (Uint32) 175, 250, 70,
                                         TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, false);
        TextureManager::Instance()->draw(std::string("YourScore"), (Uint32) 190, (Uint32) 270, 320, 90,
                                         TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, false);
        TextureManager::Instance()->draw(std::string("ScoreText"), (Uint32) 650, (Uint32) 275, 500, 70,
                                         TheGame::Instance()->getRenderer(),
                                         SDL_FLIP_NONE, false);
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->draw();
        }
    }
}

bool WinState::onEnter() {

    // parse the state
    StateParser stateParser;
    stateParser.parseState("../config/states.xml", s_winID, &m_gameObjects, &m_textureIDList, m_musicID, nullptr,
                           nullptr);
    m_callbacks.push_back(0);
    m_callbacks.push_back(s_winToMain);
    m_callbacks.push_back(s_restartPlay);
    // set the callbacks for menu items
    setCallbacks(m_callbacks);

    std::string messageScore = std::to_string(TheGame::Instance()->getCurrentScore()) + " / " +
                               std::to_string(TheGame::Instance()->getMaxScore());

    std::string messageKilled = std::to_string(TheGame::Instance()->getTotalKilledEnemies()) + " / " +
                                std::to_string(TheGame::Instance()->getTotalEnemies());


    LOG(INFO) << "Message Score: " << messageScore << "\t" << "Message Killed: " << messageKilled;

    const std::string FontFile = std::string("../content/fonts/zorque.ttf");
    SDL_Color FontColor = {255, 255, 255, 255};
    int fontSize = 40;
    SDL_Texture *tmpS = TheTextManager::Instance()->renderText(messageScore, FontFile, FontColor, fontSize,
                                                               TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->loadFromImage(std::string("ScoreText"), tmpS, TheGame::Instance()->getRenderer());
    SDL_Texture *tmpK = TheTextManager::Instance()->renderText(messageKilled, FontFile, FontColor, fontSize,
                                                               TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->loadFromImage(std::string("KilledText"), tmpK, TheGame::Instance()->getRenderer());

    m_loadingComplete = true;

    if (!TheSoundManager::Instance()->isPlayingMusic()) {
        TheSoundManager::Instance()->playMusic(m_musicID, -1);
    }
    else {
        while (!Mix_FadeOutMusic(1500) && Mix_PlayingMusic()) {
            // wait for any fades to complete
            SDL_Delay(100);
        }
        TheSoundManager::Instance()->playMusic(m_musicID, -1);
    }

    return true;
}

bool WinState::onExit() {
    m_exiting = true;
    LOG(INFO) << "[WinState.cpp]-WinState::onExit()";
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            delete m_gameObjects[i];
            m_gameObjects.erase(m_gameObjects.begin() + i);
        }
        m_gameObjects.clear();
    }

    // clear the texture manager
    if (!m_textureIDList.empty()) {
        for (int i = 0; i < m_textureIDList.size(); i++) {
            TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
        }
    }
    TheTextureManager::Instance()->clearFromTextureMap("ScoreText");
    TheTextureManager::Instance()->clearFromTextureMap("KilledText");
    TheInputHandler::Instance()->reset();

    return true;
}

void WinState::setCallbacks(const std::vector<Callback> &callbacks) {
    if (!m_gameObjects.empty()) {
        // go through the game objects
        for (int i = 0; i < m_gameObjects.size(); i++) {
            // if they are of type MenuButton then assign a callback based on the id passed in from the file
            if (dynamic_cast<MenuButton *>(m_gameObjects[i])) {
                MenuButton *pButton = dynamic_cast<MenuButton *>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}

void WinState::s_winToMain() {
    TheSoundManager::Instance()->setRestart(true);
    TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void WinState::s_restartPlay() {
    TheSoundManager::Instance()->setRestart(true);
    TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}