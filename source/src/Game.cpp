#include "Game.h"
#include "InputHandler.h"
#include "TextManager.h"
#include "MainMenuState.h"
#include "MenuButton.h"
#include "MenuSelect.h"
#include "BackgroundTile.h"
#include "AnimatedGraphic.h"
#include "ScrollingBackground.h"
#include "GameOverState.h"
#include "PlayState.h"
#include "Enemy.h"
#include "../../lib/loggers/easylogging++.h"

Game *Game::s_pInstance = 0;

Game::Game() : m_pWindow(0), m_pRenderer(0), m_bRunning(false), m_pGameStateMachine(0), m_playerLives(3),
               m_bLevelComplete(false), m_currentScore(0), m_maxScore(0) {
    m_totalEnemies = m_killedEnemies = m_remainingEnemies = 0;
    // add some level files to an array
    m_levelFiles.push_back("../content/levelMap.tmx");
    // start at this level
    m_currentLevel = 1;
}

Game::~Game() {
    // we must clean up after ourselves to prevent memory leaks
    m_pRenderer = 0;
    m_pWindow = 0;
}

GameObjectFactory *GameObjectFactory::s_pInstance = 0;

void Game::registerAllGameTypes() {

    TheGameObjectFactory::Instance()->registerType("AnimatedGraphic", new AnimatedGraphicCreator());
    TheGameObjectFactory::Instance()->registerType("BackgroundTile", new BackgroundTileCreator());
    TheGameObjectFactory::Instance()->registerType("MenuButton", new MenuButtonCreator());
    TheGameObjectFactory::Instance()->registerType("MenuSelect", new MenuSelectCreator());
    TheGameObjectFactory::Instance()->registerType("Player", new PlayerCreator());
    TheGameObjectFactory::Instance()->registerType("Enemy", new EnemyCreator());
    TheGameObjectFactory::Instance()->registerType("ScrollingBackground", new ScrollingBackgroundCreator());
}

bool Game::init(const char *title, int xpos, int ypos, int width, int height, bool fullscreen, bool music, bool sfx) {

    LOG(INFO) << "[Game.cpp] Game::init";
    // TODO:We can parametrize this function for more window flags on SDL_CreateWindow.

    // store the game width and height
    m_gameWidth = width;
    m_gameHeight = height;

    Uint32 flags = 0;

    if (fullscreen) {
        flags = SDL_WINDOW_FULLSCREEN;
    }

    // attempt to initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        LOG(INFO) << "[Game.cpp]-Game::init SDL initialization successful";
        if (TheTextManager::Instance()->initTTF() == 1) {
            // init the window
            m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flags);

            if (m_pWindow != 0) { // window init success
                LOG(INFO) << "[Game.cpp]-Game::init Window creation successful";
                m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

                if (m_pRenderer != 0) { // renderer init success
                    LOG(INFO) << "[Game.cpp]-Game::init Renderer creation successful";
                    SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);
                }
                else {
                    LOG(FATAL) << "[Game.cpp]-Game::init Renderer creation failled";
                    return false; // renderer init fail
                }
            }
            else {
                LOG(FATAL) << "[Game.cpp]-Game::init Window creation failled";
                return false; // window init fail
            }
        }
        else {
            LOG(FATAL) << "[Game.cpp]-Game::init TFF initialization failled";
            return false; // ttf init fail
        }
    }
    else {
        LOG(FATAL) << "[Game.cpp]-Game::init SDL initialization failled";
        return false; // SDL init fail
    }

    registerAllGameTypes();

    m_musicEnabled = music;
    m_sfxEnabled = sfx;
    m_difficulty = 0;

    m_pGameStateMachine = new GameStateMachine();
    m_pGameStateMachine->changeState(new MainMenuState());

    LOG(INFO) << "[Game.cpp]-Game::init success";

    m_bRunning = true; // everything initialized successfully, start the main loop

    return true;
}

void Game::setCurrentLevel(int currentLevel) {
    LOG(INFO) << "[Game.cpp]-Game::setCurrentLevel";
    m_currentLevel = currentLevel;
    m_pGameStateMachine->changeState(new GameOverState());
    m_bLevelComplete = false;
}

void Game::render() {
    SDL_RenderClear(m_pRenderer); // clear the renderer to the draw color

    m_pGameStateMachine->render();

    SDL_RenderPresent(m_pRenderer); // draw to screen
}

void Game::clean() {

    TheInputHandler::Instance()->clean();

    m_pGameStateMachine->clean();

    m_pGameStateMachine = 0;
    delete m_pGameStateMachine;

    TheTextureManager::Instance()->clearTextureMap();

    LOG(INFO) << "[Game.cpp]-Game::clean cleaning the game...";
    SDL_DestroyWindow(m_pWindow);
    SDL_DestroyRenderer(m_pRenderer);
    SDL_Quit();
}

void Game::update() {
    m_pGameStateMachine->update();
}

void Game::handleEvents() {
    TheInputHandler::Instance()->update();

    if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RETURN)) {
        m_pGameStateMachine->changeState(new PlayState());
    }
}