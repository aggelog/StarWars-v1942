#include "PlayState.h"
#include "LargeEnemy.h"
#include "MediumEnemy.h"
#include "SmallEnemy.h"
#include "GameOverState.h"
#include "WinState.h"
#include "PauseState.h"
#include "Game.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "BulletHandler.h"
#include "../../lib/loggers/easylogging++.h"
#include "SoundManager.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update() {

    if (m_loadingComplete && !m_exiting) {
        if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE)) {
            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
        }

        m_TimerNow = SDL_GetTicks();

        handleSpawns();

        m_collisionManager.checkPlayerEnemyCollision(pLevel->getPlayer(), m_gameObjects);
        m_collisionManager.checkPlayerEnemyBulletCollision(m_gameObjects);
        m_collisionManager.checkEnemyPlayerBulletCollision(pLevel->getPlayer(), m_gameObjects);

        if (TheGame::Instance()->getPlayerLives() == 0) {
            TheGame::Instance()->getStateMachine()->pushState(new GameOverState());
            return;
        }

        if (pLevel != 0) {
            if (!m_gameObjects.empty()) {
                for (int i = 0; i < m_gameObjects.size(); i++) {
                    if (m_gameObjects[i]->dead() ||
                        (dynamic_cast<Enemy *>(m_gameObjects[i])->getPosition().getY() < -150) ||
                        (dynamic_cast<Enemy *>(m_gameObjects[i])->getPosition().getY() >
                         TheGame::Instance()->getGameHeight())) {
                        delete m_gameObjects[i];
                        m_gameObjects.erase(m_gameObjects.begin() + i);
                        TheGame::Instance()->decreaseRemainingEnemies();
                        continue;
                    }
                    m_gameObjects[i]->update();
                }
            }
            pLevel->update();
        }

        if (m_TimerNow >= pLevel->getEndedAt() || TheGame::Instance()->getRemainingEnemies() == 0) {
            TheGame::Instance()->getStateMachine()->pushState(new WinState());
            return;
        }

    }
}

void PlayState::render() {
    if (m_loadingComplete) {
        if (pLevel != 0) {
            pLevel->render();
            if (!m_gameObjects.empty()) {
                for (int i = 0; i < m_gameObjects.size(); i++) {
                    m_gameObjects[i]->draw();
                }
            }
            drawPlayersLivesLoops();
        }
    }
}

bool PlayState::onEnter() {
    m_TimerNow = m_LargeTimerLast = m_MediumTimerLast = m_SmallTimerLast = 0;
    // parse the state
    std::string tmp = "";
    StateParser stateParser;
    LevelParser levelParser;
    pLevel = levelParser.parseLevel(
            TheGame::Instance()->getLevelFiles()[TheGame::Instance()->getCurrentLevel() - 1].c_str());

    stateParser.parseState("../config/states.xml", s_playID, nullptr, &m_textureIDList, tmp, nullptr,
                           pLevel->getEnemySettings());

    pLevel->setTotalRemainingEnemies();
    TheGame::Instance()->resetScoreKilled();
    TheBulletHandler::Instance()->cleanAllBullets();

    if (pLevel != 0) {
        m_loadingComplete = true;
        if (TheSoundManager::Instance()->getRestart()) {
            if (!TheSoundManager::Instance()->isPlayingMusic()) {
                TheSoundManager::Instance()->playMusic(std::string("menuMusic"), -1);
            }
            else {
                while (!Mix_FadeOutMusic(1500) && Mix_PlayingMusic()) {
                    // wait for any fades to complete
                    SDL_Delay(100);
                }
                TheSoundManager::Instance()->playMusic(std::string("menuMusic"), -1);
            }
        }
    }

    LOG(INFO) << "[PlayState.cpp]-PlayState::onEnter() : entering PlayState";
    return true;
}

bool PlayState::onExit() {

    m_exiting = true;
    TheInputHandler::Instance()->reset();

    LOG(INFO) << "[PlayState.cpp]-PlayState::onExit() : exiting PlayState";
    return true;
}

void PlayState::drawPlayersLivesLoops() {

    int width = TheGame::Instance()->getGameWidth();
    int height = TheGame::Instance()->getGameHeight();


    int currentFrame = (SDL_GetTicks() / 200) % 9;

    for (int i = 0; i < TheGame::Instance()->getPlayerLives(); i++) {

        TextureManager::Instance()->drawFrame("playerLives",
                                              (Uint32) 15 + (i * 50), (Uint32) height - 75,
                                              65, 66, 1, currentFrame,
                                              TheGame::Instance()->getRenderer());
    }

    for (int i = 0; i < pLevel->getPlayer()->getRemainingLoops(); i++) {

        TextureManager::Instance()->drawFrame("playerLoops",
                                              (Uint32) (width - 75) - (i * 50), (Uint32) height - 90,
                                              63, 85, 1, currentFrame,
                                              TheGame::Instance()->getRenderer());
    }
}

void PlayState::handleSpawns() {

    if (((m_TimerNow - m_LargeTimerLast) >= pLevel->getEnemySettingsByType("lenemy").spawnFrequency) &&
        (pLevel->getEnemySettingsByType("lenemy").num > 0)) {

        m_LargeTimerLast = m_TimerNow;
        LargeEnemy *newLargeEnemy = new LargeEnemy(pLevel->getEnemySettingsByType("lenemy"));
        m_gameObjects.push_back(newLargeEnemy);


        pLevel->decreaseEnemyNumberByType(1, "lenemy");
        LOG(INFO) << "Large Enemy Created";
    }

    if (((m_TimerNow - m_MediumTimerLast) >= pLevel->getEnemySettingsByType("menemy").spawnFrequency) &&
        (pLevel->getEnemySettingsByType("menemy").num > 0)) {

        m_MediumTimerLast = m_TimerNow;
        MediumEnemy *newMediumEnemy = new MediumEnemy(pLevel->getEnemySettingsByType("menemy"));
        m_gameObjects.push_back(newMediumEnemy);
        pLevel->decreaseEnemyNumberByType(1, "menemy");
        LOG(INFO) << "Medium Enemy Created";
    }

    if (((m_TimerNow - m_SmallTimerLast) >= pLevel->getEnemySettingsByType("senemy").spawnFrequency) &&
        (pLevel->getEnemySettingsByType("senemy").num > 0)) {

        m_SmallTimerLast = m_TimerNow;
        SmallEnemy *newSmallEnemy = new SmallEnemy(pLevel->getEnemySettingsByType("senemy"));
        m_gameObjects.push_back(newSmallEnemy);
        pLevel->decreaseEnemyNumberByType(1, "senemy");
        LOG(INFO) << "Small Enemy Created";
    }
}