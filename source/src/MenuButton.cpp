#include "MenuButton.h"
#include "InputHandler.h"
#include "../../lib/loggers/easylogging++.h"

void MenuButton::load(std::unique_ptr<LoaderParams> const &pParams) {
    SDLGameObject::load(std::move(pParams));
    m_callbackID = pParams->getCallbackID();
    m_currentFrame = MOUSE_OUT;
    m_bReleased = true;
    m_bg = false;
}

void MenuButton::draw() {
    SDLGameObject::draw(); // use the base class drawing
}

void MenuButton::update() {
    Vector2D *pMousePos = TheInputHandler::Instance()->getMousePosition();

    if (pMousePos->getX() < (m_position.getX() + m_width) && pMousePos->getX() > m_position.getX() &&
        pMousePos->getY() < (m_position.getY() + m_height) && pMousePos->getY() > m_position.getY()) {

        m_currentFrame = MOUSE_OVER;

        if (TheInputHandler::Instance()->getMouseButtonState(LEFT) && m_bReleased) {
            m_currentFrame = CLICKED;
            m_bReleased = false;
        }
        else if (TheInputHandler::Instance()->getMouseButtonState(LEFT) && !m_bReleased) {
            m_currentFrame = CLICKED;
        }
        else if (!TheInputHandler::Instance()->getMouseButtonState(LEFT) && m_bReleased) {
            m_currentFrame = MOUSE_OVER;
        }
        else if (!TheInputHandler::Instance()->getMouseButtonState(LEFT) && !m_bReleased) {
            m_callback(); // call our callback function
            m_bReleased = true;
        }
    }
    else {
        m_currentFrame = MOUSE_OUT;

        if (TheInputHandler::Instance()->getMouseButtonState(LEFT) && m_bReleased) {
            m_bReleased = false;
        }
        if (!TheInputHandler::Instance()->getMouseButtonState(LEFT) && !m_bReleased) {
            m_bReleased = true;
        }
    }
}

void MenuButton::clean() {
    SDLGameObject::clean();
}