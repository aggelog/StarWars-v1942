#include <string>
#include "BulletHandler.h"
#include "../../lib/loggers/easylogging++.h"

BulletHandler *BulletHandler::s_pInstance = 0;

void BulletHandler::addPlayerBullet(int x, int y, int width, int height, std::string textureID, int numFrames,
                                    Vector2D heading) {

    PlayerBullet *pPlayerBullet = new PlayerBullet();

    pPlayerBullet->load(std::unique_ptr<LoaderParams>(new LoaderParams(x, y, width, height, textureID, numFrames)),
                        heading);
    m_playerBullets.push_back(pPlayerBullet);

}

void BulletHandler::addEnemyBullet(int x, int y, int width, int height, std::string textureID, int numFrames,
                                   Vector2D heading) {

    EnemyBullet *pEnemyBullet = new EnemyBullet();

    pEnemyBullet->load(std::unique_ptr<LoaderParams>(new LoaderParams(x, y, width, height, textureID, numFrames)),
                       heading);
    m_enemyBullets.push_back(pEnemyBullet);
}

void BulletHandler::update(std::string type) {
    if (type == std::string("player")) {
        for (std::vector<PlayerBullet *>::iterator p_it = m_playerBullets.begin(); p_it != m_playerBullets.end();) {
            if ((*p_it)->getPosition().getY() + 115 < 0 || (*p_it)->dead()) {
                delete *p_it;
                p_it = m_playerBullets.erase(p_it);
            } else {
                (*p_it)->update();
                ++p_it;
            }
        }
    }
    if (type == std::string("enemy")) {
        for (std::vector<EnemyBullet *>::iterator p_it = m_enemyBullets.begin(); p_it != m_enemyBullets.end();) {
            if ((*p_it)->getPosition().getY() + 115 < 0 || (*p_it)->dead()) {
                delete *p_it;
                p_it = m_enemyBullets.erase(p_it);
            } else {
                (*p_it)->update();
                ++p_it;
            }
        }
    }
}

void BulletHandler::draw(std::string type) {
    if (type == std::string("player")) {
        for (std::vector<PlayerBullet *>::iterator p_it = m_playerBullets.begin(); p_it != m_playerBullets.end();) {
            (*p_it)->draw();
            ++p_it;
        }
    }
    if (type == std::string("enemy")) {
        for (std::vector<EnemyBullet *>::iterator p_it = m_enemyBullets.begin(); p_it != m_enemyBullets.end();) {
            (*p_it)->draw();
            ++p_it;
        }
    }
}