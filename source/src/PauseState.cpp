#include "PauseState.h"
#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

const std::string PauseState::s_pauseID = "PAUSE";

void PauseState::update() {
    if (m_loadingComplete && !m_exiting &&!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->update();
        }
    }
}

void PauseState::render() {
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->draw();
        }
    }
}

bool PauseState::onEnter() {


    std::string temp = "";

    StateParser stateParser;
    stateParser.parseState("../config/states.xml", s_pauseID, &m_gameObjects, &m_textureIDList, temp, nullptr, nullptr);

    m_callbacks.push_back(0);
    m_callbacks.push_back(s_pauseToMain);
    m_callbacks.push_back(s_resumePlay);

    setCallbacks(m_callbacks);

    m_loadingComplete = true;

    if (TheSoundManager::Instance()->isPlayingMusic()) {
        TheSoundManager::Instance()->pauseMusic();
    }

    return true;
}

bool PauseState::onExit() {
    LOG(INFO) << "[PauseState.cpp]-PauseState::onExit()";
    m_exiting = true;
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->clean();
            m_gameObjects[i] = NULL;
        }
        m_gameObjects.clear();
    }

    // clear the texture manager
    /*for (int i = 0; i < m_textureIDList.size(); i++) {
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }*/

    // reset the mouse button states to false
    TheInputHandler::Instance()->reset();

    return true;
}

void PauseState::setCallbacks(const std::vector<Callback> &callbacks) {
    // go through the game objects
    if (!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            // if they are of type MenuButton then assign a callback based
            //on the id passed in from the file
            if (dynamic_cast<MenuButton *>(m_gameObjects[i])) {
                MenuButton *pButton = dynamic_cast<MenuButton *>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}

void PauseState::s_pauseToMain() {
    TheSoundManager::Instance()->setRestart(false);
    if (TheGame::Instance()->isMusicEnabled()) {
        TheSoundManager::Instance()->resumeMusic();
    }
    TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void PauseState::s_resumePlay() {
    TheSoundManager::Instance()->setRestart(false);
    if (TheGame::Instance()->isMusicEnabled()) {
        TheSoundManager::Instance()->resumeMusic();
    }
    TheGame::Instance()->getStateMachine()->popState();

}