#include "SmallEnemy.h"
#include "Game.h"

SmallEnemy::SmallEnemy(struct enemySettings settings) : Enemy() {

    srand(time(NULL));

    int typeA = rand() % 30 + 1;
    int typeB = rand() % 10 + 1;

    if (typeA <= 9) {                       // First Type
        if (typeB <= 5) {                   // Green
            m_textureID = "smallEnemyGreenFirst";
        }
        else {                              // Blue
            m_textureID = "smallEnemyBlueFirst";
        }
    }
    else if (typeA >= 10 && typeA <= 19) {  // Second Type
        if (typeB <= 5) {                   // Green
            m_textureID = "smallEnemyGreenSecond";
        }
        else {                              // Blue
            m_textureID = "smallEnemyBlueSecond";
        }
    }
    else {                                  // Third Type
        if (typeB <= 5) {                   // Green
            m_textureID = "smallEnemyGreenThird";
        }
        else {                              // Blue
            m_textureID = "smallEnemyBlueThird";
        }
    }
    m_position = Vector2D(TheGame::Instance()->getGameWidth() / 3, TheGame::Instance()->getGameHeight() - 100);
    m_velocity = Vector2D(0, -(settings.movementSpeed));

    m_currentRow = 1;
    m_currentFrame = 0;
    m_numFrames = 6;
    m_width = 64;
    m_height = 75;
    m_worthPoints = 1000;
    TheGame::Instance()->setMaxScore(m_worthPoints);
    setHp(settings.hp);
    setFrequencyBullet(settings.frequencyBullet);
    setSpeedBullet(settings.frequencyBullet);
    setMovementSpeed(settings.movementSpeed);
}

void SmallEnemy::update() {

    if (m_bDying && !m_bDead) {
        if (m_currentFrame == m_numFrames - 1) {
            m_bDead = true;
            m_bDying = false;
            return;
        }
        m_currentFrame = (SDL_GetTicks() / 500) % m_numFrames;
    }

    Enemy::update();
}

void SmallEnemy::draw() {
    Enemy::draw();
}

void SmallEnemy::collision() {
    --m_hp;

    setHp(m_hp);

    if (m_hp == 0) {
        m_bDying = true;
        TheGame::Instance()->setCurrentScore(m_worthPoints);
        TheGame::Instance()->setKilledEnemies();
    }
    m_bDying = true;
}