#include "SoundManager.h"
#include "Game.h"
#include "../../lib/loggers/easylogging++.h"

SoundManager *SoundManager::s_pInstance;

SoundManager::SoundManager() {
    Mix_OpenAudio(22050, AUDIO_S16, 2, (4096 / 2));
    m_restart = true;
}

SoundManager::~SoundManager() {
    Mix_CloseAudio();
}

bool SoundManager::load(std::string fileName, std::string id, sound_type type) {
    if (type == SOUND_MUSIC) {
        Mix_Music *pMusic = Mix_LoadMUS(fileName.c_str());
        if (pMusic == 0) {
            LOG(INFO) << "[SoundManager.cpp]-SoundManager::load Could not load music: ERROR - " << Mix_GetError();
            return false;
        }

        m_music[id] = pMusic;
        //Mix_VolumeMusic(4);
        return true;
    }
    else if (type == SOUND_SFX) {
        Mix_Chunk *pChunk = Mix_LoadWAV(fileName.c_str());
        if (pChunk == 0) {
            LOG(INFO) << "[SoundManager.cpp]-SoundManager::load Could not load SFX: ERROR - " << Mix_GetError();
            return false;
        }

        m_sfxs[id] = pChunk;
        return true;
    }
    return false;
}

void SoundManager::playMusic(std::string id, int loop) {
    if (!TheGame::Instance()->isMusicEnabled())return;
    Mix_PlayMusic(m_music[id], loop);
}

void SoundManager::playSound(std::string id, int loop) {
    if (!TheGame::Instance()->isSFXEnabled())return;
    Mix_PlayChannel(-1, m_sfxs[id], loop);
}

int SoundManager::isPlayingMusic() {
    return Mix_PlayingMusic();
}

int SoundManager::isMusicPaused() {
    return Mix_PausedMusic();
}

void SoundManager::pauseMusic() {
    Mix_PauseMusic();
}

void SoundManager::resumeMusic() {
    Mix_ResumeMusic();
}