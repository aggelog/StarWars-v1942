#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "OptionsMenuState.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "../../lib/loggers/easylogging++.h"

const std::string MainMenuState::s_menuID = "MENU";

void MainMenuState::update() {
    if (m_loadingComplete && !m_gameObjects.empty() && !m_exiting) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->update();
        }
    }
}

void MainMenuState::render() {
    if (m_loadingComplete && !m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            m_gameObjects[i]->draw();
        }
    }
}

bool MainMenuState::onEnter() {
    // parse the state
    StateParser stateParser;
    stateParser.parseState("../config/states.xml", s_menuID, &m_gameObjects, &m_textureIDList, m_musicID, nullptr,
                           nullptr);
    m_callbacks.push_back(0); //pushback 0 callbackID start from 1
    m_callbacks.push_back(s_menuToPlay);
    m_callbacks.push_back(s_menuToOptions);
    m_callbacks.push_back(s_exitFromMenu);
    // set the callbacks for menu items
    setCallbacks(m_callbacks);
    m_loadingComplete = true;

    if(TheSoundManager::Instance()->getRestart()) {
        if (!TheSoundManager::Instance()->isPlayingMusic()) {
            TheSoundManager::Instance()->playMusic(m_musicID, -1);
        }
        else {
            while (!Mix_FadeOutMusic(1500) && Mix_PlayingMusic()) {
                // wait for any fades to complete
                SDL_Delay(100);
            }
            TheSoundManager::Instance()->playMusic(m_musicID, -1);
        }
    }

    return true;
}

bool MainMenuState::onExit() {
    LOG(INFO) << "[MainMenuState.cpp]-MainMenuState::onExit()";
    m_exiting = true;

    for (int i = 0; i < m_gameObjects.size(); i++) {
        m_gameObjects[i]->clean();
        m_gameObjects[i] = NULL;
    }
    m_gameObjects.clear();

    // clear the texture manager
    for (int i = 0; i < m_textureIDList.size(); i++) {
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }

    // reset the input handler
    TheInputHandler::Instance()->reset();
    return true;
}

void MainMenuState::s_menuToPlay() {
    TheSoundManager::Instance()->setRestart(false);
    TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void MainMenuState::s_menuToOptions() {
    TheGame::Instance()->getStateMachine()->changeState(new OptionsMenuState());
}

void MainMenuState::s_exitFromMenu() {
    TheGame::Instance()->quit();
}

void MainMenuState::setCallbacks(const std::vector<Callback> &callbacks) {
    // go through the game objects
    if (!m_gameObjects.empty()) {
        for (int i = 0; i < m_gameObjects.size(); i++) {
            // if they are of type MenuButton then assign a callback
            // based on the id passed in from the file
            if (dynamic_cast<MenuButton *>(m_gameObjects[i])) {
                MenuButton *pButton = dynamic_cast<MenuButton *>(m_gameObjects[i]);
                pButton->setCallback(callbacks[pButton->getCallbackID()]);
            }
        }
    }
}